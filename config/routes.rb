Rails.application.routes.draw do
  resources :departamentos
  root 'produtos#index'
  resource :produtos, only: [:new, :create, :destroy]
  get 'produtos/busca' => 'produtos#busca', as: :busca_produto
end
